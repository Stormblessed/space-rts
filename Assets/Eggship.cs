﻿using UnityEngine;
using System.Collections;

public class Eggship : MonoBehaviour 
{
	public float EtherGainRate = 10.0f;
	public float CurrentEther = 0.0f;
	
	public StationSpawner FrightSpawner;
	public StationSpawner HorrorSpawner;
	
	public string CurrentSpawnDecision = "None";
	
	void Start() 
	{
	}
	
	void Update() 
	{
		CurrentEther += EtherGainRate * Time.deltaTime;
		if(CanSpawn()) SpawnShip();
	}
	
	private void SpawnShip()
	{
		float etherAfterPurchaseAttempt = GetDecidedSpawner().CheckPurchase(CurrentEther);
		if(etherAfterPurchaseAttempt >= 0) 
		{
			GetDecidedSpawner().AddSpawnRequest("Ship");
			CurrentEther = etherAfterPurchaseAttempt;
		}
		CurrentSpawnDecision = "None";
	}
	
	private bool CanSpawn()
	{
		return GetDecidedSpawner().ShipCost <= CurrentEther;
	}
	
	private StationSpawner GetDecidedSpawner()
	{
		if(CurrentSpawnDecision == "None") CurrentSpawnDecision = PickRandomSpawnDecision();
		else if(CurrentSpawnDecision == "Fright") return FrightSpawner;
		return HorrorSpawner;
	}
	
	private string PickRandomSpawnDecision()
	{
		int decision = Random.Range(0, 2);
		if(decision == 0) return "Fright";
		else return "Horror";
	}
}
