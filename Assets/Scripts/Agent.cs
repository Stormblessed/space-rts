﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum AgentStatus
{
	Attacking,
	Spawning,
	Harvesting,
	NoGoal,
	Movement,
	Found
}

public class Agent : MonoBehaviour 
{	
	public ObjectType Type;
	
	public float DefaultMoveSpeed = .25f;
	public float CoherenceWeight = 0.0f;
	public float SeparationWeight = 1.0f;
	
	public AgentStatus Status = AgentStatus.Movement;
	
    public List<PathNode> Path = new List<PathNode>();
        
	protected GameManager Manager;
	protected PathManager PathMapper;
    
	protected PathNode GoalNode;
	protected Vector3 GoalPosition;
	protected OreAsteroid GoalAsteroid;
    
    public float InitialSpawnTime = 2.5f;
    public float SecondarySpawnTime = 1.0f;
    public float SpawnTimer = 0.0f;
    private float SpawnSpeed;
    public Vector3 SpawnMovementDirection;
    
	void Start() 
	{
		Init();
   	}
   	
   	protected void Init()
   	{
		Manager = GameObject.FindObjectOfType<GameManager>();
		PathMapper = GameObject.FindObjectOfType<PathManager>();
		
		SpawnSpeed = DefaultMoveSpeed;
		
		if(IsType(ObjectType.Friend)) GoalPosition = Manager.Goals[0].transform.position;
		else if(IsType(ObjectType.Foe)) GoalPosition = Manager.MainStation.transform.position;
		//DeterminePath();
   	}
		
	void Update() 
	{
		if(IsType(ObjectType.Friend)) FriendUpdate();
	}
	
	protected void FriendUpdate()
	{
		if(Status == AgentStatus.Spawning) HandleSpawning();
		else if(Status == AgentStatus.Attacking) HandleAttacking();
		else if(!IsAtGoal() && Status != AgentStatus.NoGoal) MoveTowardsGoal();
		else if(Status == AgentStatus.Harvesting) HandleHarvesting(GoalAsteroid);
		else if(Status != AgentStatus.NoGoal) Status = AgentStatus.Found;
	}
	
	public void MoveTowardsGoal()
	{
		Vector3 pathForce = DetermineTargetDirection(DeterminePathTarget().Position) * DefaultMoveSpeed * Time.deltaTime;
		Debug.DrawRay(transform.position, pathForce * 100.0f, Color.red);
		Vector3 boidForce = DetermineBoidBehavior(100.0f) * Time.deltaTime;
		Debug.DrawRay(transform.position, boidForce * 100.0f, Color.yellow);
		Vector3 overallForce = pathForce + boidForce;
		
		transform.Translate(overallForce);
		TurnToFaceMovementDirection(overallForce);
	}
	
	public void MoveTowardsTarget(Vector3 closeTarget, Vector3 realTarget)
	{
		Vector3 pathForce = Vector3.zero;
		if((transform.position - realTarget).magnitude <= this.GetComponent<Ship>().WeaponRange)
			pathForce = DetermineTargetDirection(closeTarget) * (DefaultMoveSpeed / 5.0f) * Time.deltaTime;
		else 
			pathForce = DetermineTargetDirection(closeTarget) * DefaultMoveSpeed * Time.deltaTime;
		Debug.DrawRay(transform.position, pathForce * 100.0f, Color.red);
		Vector3 boidForce = DetermineBoidBehavior(100.0f) * Time.deltaTime;
		Debug.DrawRay(transform.position, boidForce * 100.0f, Color.yellow);
		Vector3 overallForce = pathForce + boidForce;
		
		transform.Translate(overallForce);
		TurnToFaceMovementDirection(overallForce);
	}
	
	public void TurnToFaceMovementDirection(Vector3 moveDirection)
	{
		Vector3 newForward = moveDirection.normalized;
		if(newForward != Vector3.zero)
			transform.FindChild("Model").transform.forward = newForward;
	}
	
	public void TurnToFaceTarget(Vector3 target)
	{
		Vector3 newForward = DetermineTargetDirection(target);
		if(newForward != Vector3.zero)
			transform.FindChild("Model").transform.forward = newForward;
	}
	
	public Vector3 DetermineBoidBehavior(float radius)
	{
		List<Agent> neighbors = GetNeighborsWithinRadius(radius);
		
		Vector3 avgCenter = transform.position;
		Vector3 separation = Vector3.zero;
		
		for(int i = 0; i < neighbors.Count; i++)
		{
			avgCenter += neighbors[i].transform.position;
			Vector3 relativePosition = (transform.position - neighbors[i].transform.position);
			if(relativePosition != Vector3.zero) separation += relativePosition / relativePosition.sqrMagnitude;
		}
		
		if(neighbors.Count != 0) avgCenter /= neighbors.Count + 1;
		Vector3 coherency = avgCenter - transform.position;
		
		//Debug.DrawRay(transform.position, separation, Color.green);
		Debug.DrawRay(transform.position, coherency, Color.magenta);
		return (coherency * CoherenceWeight) + (separation * SeparationWeight);
	}
	
	public bool IsAtGoal()
	{
		float acceptableDistance;
		if(Status == AgentStatus.Movement)
		{
			acceptableDistance = 0.3f;
			return Vector3.Magnitude(GoalPosition - transform.position) <= acceptableDistance;
		}
		else if(Status == AgentStatus.Harvesting)
		{
			acceptableDistance = 2.5f;
			return Vector3.Magnitude(GoalPosition - transform.position) <= acceptableDistance;
		}
		else if(Status == AgentStatus.Found) return true;
		else return false;
	}
	
	public void GiveMovementGoal(Vector3 position)
	{
		if(Status == AgentStatus.Harvesting) this.GetComponent<Ship>().StopHarvesting();
		Status = AgentStatus.Movement;
		GoalPosition = position;
		DeterminePath();
	}
	
	public void HandleAttacking()
	{
		if(this.GetComponent<Ship>().AttackTargets.Count > 0) HandleAttackingTargets();
		else HandleNoTargetAttacking();
	}
	
	public void HandleAttackingTargets()
	{
		Agent closestAgent = GetClosestAgent(this.GetComponent<Ship>().AttackTargets);
		if(closestAgent == null) return;
		MoveTowardsTarget(DetermineEffectiveAttackLocation(closestAgent.transform.position), closestAgent.transform.position);
		TurnToFaceTarget(closestAgent.transform.position);
	}
	
	public Vector3 DetermineEffectiveAttackLocation(Vector3 targetPosition)
	{
		Ship agentShip = this.GetComponent<Ship>();
		
		Vector3 attackLocation = targetPosition + -DetermineTargetDirection(targetPosition) * (agentShip.WeaponRange * 0.8f);
		return attackLocation;
	}
	
	public void HandleNoTargetAttacking()
	{
		List<Agent> targets = GetNeighborsOfTypeWithinRadius(GetOppositeType(), this.GetComponent<Ship>().AttackRadius);
		if(targets.Count > 0)
		{
			this.GetComponent<Ship>().AttackTargets.Add(GetClosestAgent(targets));
			HandleAttackingTargets();
		}
		else Status = AgentStatus.NoGoal;
	}
	
	public Agent GetClosestAgent(List<Agent> agents)
	{
		Agent closestTarget = agents[0];
		float currentClosestSqrDistance = float.MaxValue;
		for(int i = 0; i < agents.Count; i++)
		{
			if(agents[i] == null) continue;
			float sqrDistance = (agents[i].transform.position - this.transform.position).sqrMagnitude;
			if(sqrDistance <= currentClosestSqrDistance) 
			{
				currentClosestSqrDistance = sqrDistance;
				closestTarget = agents[i];
			}
		}
		return closestTarget;
	}
	
	public void GiveHarvestGoal(OreAsteroid asteroid)
	{
		Status = AgentStatus.Harvesting;
		GoalPosition = asteroid.transform.position;
		GoalAsteroid = asteroid;
		DeterminePath();
	}
	
	public void HandleHarvesting(OreAsteroid asteroid)
	{
		this.GetComponent<Ship>().Harvest(asteroid);
	}
	
	protected void HandleSpawning()
	{
		SpawnTimer += Time.deltaTime;
		if(SpawnTimer <= InitialSpawnTime) HandleInitialSpawning();
		else if(SpawnTimer <= SecondarySpawnTime) HandleSecondarySpawning();
		else Status = AgentStatus.NoGoal;
	}
	
	protected void HandleInitialSpawning()
	{
		Vector3 overallForce = SpawnMovementDirection * SpawnSpeed * Time.deltaTime;
		
		transform.Translate(overallForce);
		TurnToFaceMovementDirection(overallForce);
	}
	
	protected void HandleSecondarySpawning()
	{
		Vector3 overallForce = SpawnMovementDirection * (SpawnSpeed / 2.0f) * Time.deltaTime;
		
		transform.Translate(overallForce);
		TurnToFaceMovementDirection(overallForce);
	}
	
	public Vector3 DetermineTargetDirection(Vector3 target)
	{
		return Vector3.Normalize(target - transform.position);
	}
	
	public PathNode DeterminePathTarget()
	{
		PathNode currentPathNode = new PathNode(GoalPosition);
		
		int i = 0;
		while(!CanSeePoint(currentPathNode.Position) && i < Path.Count)
		{
			currentPathNode = Path[i];
			i++;
		}
		return currentPathNode;
	}
    
    public void DeterminePath()
    {
		GoalNode = PathMapper.GetClosestPathNode(GoalPosition);
    	Path.Clear();
    	PathMapper.Reset();
    	
    	PathNode endNode = WeightedAstarPath(1.0f);
    	if(endNode == null) return;
    	
    	PathNode currentNode = endNode;
    	while(currentNode != null)
    	{
    		Path.Add(currentNode);
    		currentNode = currentNode.Parent;
    	}
    }
    
    public PathNode WeightedAstarPath(float weight)
    {
        HashSet<PathNode> explored = new HashSet<PathNode>();
    	List<PathNode> frontier = new List<PathNode>();
    	
    	PathNode startNode = GetClosestPathNode();
    	
    	if(startNode == GoalNode) return startNode;
    	
    	frontier.Add(startNode);
    	
    	while(frontier.Count > 0)
    	{
    		PathNode node = PathMapper.GetTopNode(frontier);
    		
    		if(explored.Contains(node)) continue;
    		if(node == GoalNode) return node;
    		
    		frontier.Remove(node);
    		explored.Add(node);
    		
    		for(int i = 0; i < node.Neighbors.Count; i++)
    		{
    			PathNode neighbor = node.Neighbors[i];
    			float TentativeScore = node.PathCost + node.DistanceToPoint(neighbor.Position);
    			
    			if(explored.Contains(neighbor) && TentativeScore >= neighbor.PathCost) continue;
    			
    			if(!frontier.Contains(neighbor) || TentativeScore < neighbor.PathCost)
    			{
    				neighbor.Parent = node;
    				neighbor.PathCost = TentativeScore;
    				neighbor.TotalCost = neighbor.PathCost + weight * neighbor.DistanceToPoint(GoalNode.Position);
    				if(!frontier.Contains(neighbor))
    					frontier.Add(neighbor);
    			}
    		}
    	}
    	
    	return null;
    }
    
    public PathNode GetClosestPathNode()
    {
    	float ShortestDistance = float.MaxValue;
    	PathNode currentClosestNode = PathMapper.PathNodes[0]; //Default value
    	for(int i = 0; i < PathMapper.PathNodes.Count; i++)
    	{
			float distance = Vector3.Magnitude(transform.position - PathMapper.PathNodes[i].Position);
			if(distance <= ShortestDistance)
    		{
    			if(CanSeePoint(PathMapper.PathNodes[i].Position))
    			{
    				ShortestDistance = distance;
    				currentClosestNode = PathMapper.PathNodes[i];
    			}
    		}
    	}
		return currentClosestNode;
    }
    
    public bool CanSeePoint(Vector3 point)
    {
		int UnitsLayerMask = ~((1 << LayerMask.NameToLayer("Ships")) | (1 << LayerMask.NameToLayer("Ore"))); 
		
		return !Physics.Linecast(this.transform.position, point, UnitsLayerMask);
    }
    
    public List<Agent> GetNeighborsWithinRadius(float radius)
    {
    	return GetNeighborsOfTypeWithinRadius(Type, radius);
    }
    
	public List<Agent> GetNeighborsOfTypeWithinRadius(ObjectType type, float radius)
	{
		List<Agent> neighborAgents = new List<Agent>();
		for(int i = 0; i < GetAllAgentsOfSimilarType(type).Count; i++)
		{
			Agent agent = GetAllAgentsOfSimilarType(type)[i];
			if(Vector3.Magnitude(this.transform.position - agent.transform.position) <= radius && agent != this && agent.Status != AgentStatus.Found) neighborAgents.Add(agent);
		}
		return neighborAgents;
	}
    
    protected List<Agent> GetAllAgentsOfSimilarType(ObjectType type)
    {
    	if(type == ObjectType.Friend) return Manager.Friends;
    	else return Manager.Foes;
    }
  	
  	public bool IsType(ObjectType type)
  	{
  		return Type == type;
  	}
  	
  	public ObjectType GetOppositeType()
  	{
  		if(Type == ObjectType.Friend) return ObjectType.Foe;
  		else return ObjectType.Friend;
  	}
    
	void OnDrawGizmos()
	{
		for(int i = 0; i < Path.Count; i++)
		{
			PathNode node = Path[i];
			
			if(i > 0)
			{
				Gizmos.color = Color.blue;
				Gizmos.DrawLine(node.Position, Path[i-1].Position);
			}
			
			Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(Path[i].Position, 0.5f);
        }
    }
}
