﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CommandManager : MonoBehaviour 
{
	public Text SelectedObjectsText;

	private List<Agent> SelectedAgents = new List<Agent>();
	private Station SelectedStation;
	
	private GameManager Manager;

	void Start() 
	{
		Manager = GameObject.FindObjectOfType<GameManager>();
	}
	
	void Update() 
	{
	}
	
	void OnGUI()
	{
		string selectedText = "";
		for(int i = 0; i < SelectedAgents.Count; i++)
		{
			selectedText += SelectedAgents[i].GetComponent<Ship>().Class + "\n";
		}
		if(IsStationSelected()) selectedText = "Station\n";
		else if(SelectedAgents.Count <= 0) selectedText = "None\n";
		SelectedObjectsText.text = selectedText;
	}
	
	public void SelectStation(Station selected)
	{
		SelectedAgents.Clear();
		SelectedStation = selected;
	}
	
	public bool IsStationSelected(Station selected)
	{
		return SelectedStation == selected;
	}
	
	public bool IsStationSelected()
	{
		return SelectedStation != null;
	}
	
	public void DeselectStation()
	{
		SelectedStation = null;
	}
	
	public void SelectAgent(Agent selected)
	{
		DeselectStation();
		SelectedAgents.Clear();
		SelectedAgents.Add(selected);
	}
	
	public void SelectAgents(List<Agent> selected)
	{
		DeselectStation();
		SelectedAgents.Clear();
		SelectedAgents.AddRange(selected);
	}
	
	public void AppendAgent(Agent appended)
	{
		DeselectStation();
		SelectedAgents.Add(appended);
	}
	
	public void AppendAgents(List<Agent> appended)
	{
		DeselectStation();
		SelectedAgents.AddRange(appended);
	}
	
	public void IssueMoveCommand(Vector3 position)
	{
		for(int i = 0; i < SelectedAgents.Count; i++)
		{
			SelectedAgents[i].GiveMovementGoal(position);
		}
	}
	
	public void IssueHarvestCommand(OreAsteroid asteroid)
	{
		for(int i = 0; i < SelectedAgents.Count; i++)
		{
			SelectedAgents[i].GiveHarvestGoal(asteroid);
		}
	}
	
	public void IssueAttackCommand(Enemy target)
	{
		for(int i = 0; i < SelectedAgents.Count; i++)
		{
			SelectedAgents[i].GetComponent<Ship>().AttackTargets.Add(target.GetComponent<Agent>());
			SelectedAgents[i].Status = AgentStatus.Attacking;
		}
	}
	
	public void IssueAttackCommand(List<Enemy> targets)
	{
		for(int i = 0; i < SelectedAgents.Count; i++)
		{
			for(int j = 0; j < targets.Count; j++)
			{
				SelectedAgents[i].GetComponent<Ship>().AttackTargets.Add(targets[j].GetComponent<Agent>());
			}
		}
	}
	
	public Vector3 GetSelectedAgentsCenter()
	{
		Vector3 avgCenter = new Vector3();
		
		for(int i = 0; i < SelectedAgents.Count; i++)
		{
			if(i == 0) avgCenter = SelectedAgents[i].transform.position;
			else avgCenter += SelectedAgents[i].transform.position;
		}
		
		if(SelectedAgents.Count != 0) avgCenter /= SelectedAgents.Count;
		
		return avgCenter;
	}
	
	public int GetNumberOfSelectedAgents()
	{
		return SelectedAgents.Count;
	}
	
	public void HandleShipClicked(Ship clicked)
	{
		if(clicked.Type == ObjectType.Friend) 
		{
			if(Input.GetKey(KeyCode.LeftControl))
				SelectAgents(Manager.Friends);
			else SelectAgent(clicked.GetComponent<Agent>());
		}
	}
	
	public bool Contains(Agent clicked)
	{
		return SelectedAgents.Contains(clicked);
	}
	
	public void Remove(Agent agent)
	{
		SelectedAgents.Remove(agent);
	}

	public List<Agent> GetSelectedAgenets() {
		return SelectedAgents;
	}
}
