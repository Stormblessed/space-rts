﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ControlManager : MonoBehaviour
{
	public LineRenderer PlaneLine;
	public LineRenderer DiagonalLine;
	public LineRenderer VerticalLine;

	public Plane MovementPlane;
	public float MaximumY = 30.0f;
	public float MinimumY = -30.0f;
	public float HeightChangeDivider = 8000.0f;
	
	private Vector3 InitialMousePosition;
	private float HeightModifier = 0.0f;

	private GameManager Manager;
	private CommandManager Commander;
	private OrbitCamera MainCamera;
	private MovementCursor Cursor;
	
	private bool MovementControlsOn = false;
	private bool HeightControlsOn = false;
	
	public static Rect BoxSelection = new Rect(0,0,0,0);
	private Vector3 LeftClick = -Vector2.one;
		
	void Start() 
	{
		Manager = GameObject.FindObjectOfType<GameManager>();
		Commander = GameObject.FindObjectOfType<CommandManager>();
		MainCamera = GameObject.FindObjectOfType<OrbitCamera>();
		Cursor = GameObject.FindObjectOfType<MovementCursor>();
		
		MovementPlane = new Plane(new Vector3(.0f, .0f, .0f), new Vector3(1.0f, .0f, .0f), new Vector3(1.0f, .0f, 1.0f));
	}
	
	void Update() 
	{
		UpdateMovementPlane();
		if(MovementControls()) HandleAllControls();
		HandleBoxSelect();
	}
	
	void OnGUI()
	{
		if(IsBoxSelectionOccurring()) 
		{
			GUI.backgroundColor = new Color (150, 150, 150, 0.5f);
			GUI.Box(BoxSelection, "");
        }
    }
    
	private void HandleBoxSelect() 
	{
		if(Input.GetMouseButtonDown(0)) 
		{
			LeftClick = Input.mousePosition;
		}
		else if(Input.GetMouseButton(0)) 
		{
			if(IsBoxSelectionOccurring())
				BoxSelection = new Rect(LeftClick.x, HelpInvertMouseY(LeftClick.y), Input.mousePosition.x - LeftClick.x, HelpInvertMouseY(Input.mousePosition.y) - HelpInvertMouseY(LeftClick.y));
        }
        else if(Input.GetMouseButtonUp(0)) 
		{
			if(IsBoxSelectionOccurring()) HandleSelectingBoxedShips();
			ResetBoxSelect();
		}
    }
    
    private bool IsBoxSelectionOccurring()
    {
		return LeftClick != -Vector3.one && (LeftClick - Input.mousePosition).magnitude > 5.0f;
    }
    
	private void HandleSelectingBoxedShips()
	{
		List<Agent> agentsInBox = new List<Agent>();
		for(int i = 0; i < Manager.Friends.Count; i++)
		{
			Vector3 positionOnScreen = Camera.main.WorldToScreenPoint(Manager.Friends[i].transform.position);
			positionOnScreen.y = HelpInvertMouseY(positionOnScreen.y);
			if(BoxSelection.Contains(positionOnScreen)) agentsInBox.Add(Manager.Friends[i]);
		}
		Commander.SelectAgents(agentsInBox);
	}
    
    private void ResetBoxSelect()
    {
		if(BoxSelection.width < 0) 
		{
			BoxSelection.x += BoxSelection.width;
			BoxSelection.width = -BoxSelection.width;
		}
		if(BoxSelection.height < 0) 
		{
			BoxSelection.y += BoxSelection.height;
			BoxSelection.height = -BoxSelection.height;
        }
		LeftClick = -Vector3.one;
    }
    
    private void UpdateMovementPlane()
	{
		MovementPlane = new Plane(new Vector3(.0f, MainCamera.Target.position.y, .0f), new Vector3(1.0f, MainCamera.Target.position.y, .0f), new Vector3(1.0f, MainCamera.Target.position.y, 1.0f));
	}
	
	private bool MovementControls()
	{
		if(Input.GetKeyDown(KeyCode.M) && !MovementControlsOn) 
		{
			HeightModifier = 0.0f;
			MovementControlsOn = true;
		}
		else if(Input.GetKeyDown(KeyCode.M) && MovementControlsOn)
		{
			HeightModifier = 0.0f;
			MovementControlsOn = false;
		}	
		return MovementControlsOn;
	}
	
	private bool HeightControls()
	{
		if(Input.GetKey(KeyCode.LeftShift) && !HeightControlsOn) 
		{
			HeightControlsOn = true;
			InitialMousePosition = Input.mousePosition;
		}
		else if(!Input.GetKey(KeyCode.LeftShift))
		{
			HeightControlsOn = false;
		}
		
		return HeightControlsOn;
	}
	
	private void HandleAllControls()
	{
		HandleMovementControls();
		if(HeightControls()) HandleHeightControls();
		if(Input.GetMouseButtonDown(1)) HandleMovementRequest();
	}
	
	private void HandleMovementControls()
	{
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		
		float rayDistance;
		if(MovementPlane.Raycast(ray, out rayDistance)) HandleMovementCursor(ray.GetPoint(rayDistance));
	}
	
	private void HandleHeightControls()
	{
		if(!Cursor.gameObject.activeSelf) Cursor.gameObject.SetActive(true);
		
		float yDiff = Input.mousePosition.y - InitialMousePosition.y;
		HeightModifier = yDiff / HeightChangeDivider;
	}
	
	private void HandleMovementRequest()
	{
		Vector3 position = Cursor.transform.position;
		Commander.IssueMoveCommand(position);
		
		HeightModifier = 0.0f;
		MovementControlsOn = false;
	}
	
	private void HandleMovementCursor(Vector3 position)
	{
		if(!Cursor.gameObject.activeSelf) Cursor.gameObject.SetActive(true);
		
		Vector3 newPosition = new Vector3(position.x, position.y + HeightModifier, position.z);
		Cursor.transform.position = newPosition;
		Cursor.GetComponent<LineCircle>().MoveTo(newPosition);
		
		SetLinePosition(PlaneLine, Commander.GetSelectedAgentsCenter(), position);
		SetLinePosition(DiagonalLine, Commander.GetSelectedAgentsCenter(), newPosition);
		SetLinePosition(VerticalLine, position, newPosition);
	}
	
	private void SetLinePosition(LineRenderer line, Vector3 posA, Vector3 posB)
	{
		line.SetPosition(0, posA);
		line.SetPosition(1, posB);
	}
	
	public static float HelpInvertMouseY(float y) 
	{
		return Screen.height - y;
    }
}
