﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Enemy : Agent 
{
	public float SlowSpeed = 1.2f;
	private float SavedNoticeSpeed;
	public float PlayerNoticeRadius = 50.0f;
	
	private CommandManager Commander;
	
	void Start()
	{
		base.Init();
		SavedNoticeSpeed = DefaultMoveSpeed;
		DefaultMoveSpeed = SlowSpeed;
		Commander = GameObject.FindObjectOfType<CommandManager>();
	}
	
	void Update() 
	{
		DetermineEnemySpeed();
		if(ShouldAttackPlayerShips()) Status = AgentStatus.Attacking;
	
		if(Status == AgentStatus.Spawning) HandleSpawning();
		else if(Status == AgentStatus.Attacking) HandleAttacking();
		else if(!IsAtGoal() && Status != AgentStatus.NoGoal) MoveTowardsGoal();
		else if(Status != AgentStatus.NoGoal) Status = AgentStatus.Found;
		else Status = AgentStatus.Movement;
	}
	
	void OnMouseDown()
	{
		if(Input.GetMouseButtonDown(0)) Commander.IssueAttackCommand(this);
	}
	
	public void DetermineEnemySpeed()
	{
		if(NoticedPlayerShips()) DefaultMoveSpeed = SavedNoticeSpeed;
		else DefaultMoveSpeed = SlowSpeed;
	}
	
	public bool NoticedPlayerShips()
	{
		List<Agent> playerShips = Manager.Friends;
		for(int i = 0; i < playerShips.Count; i++)
		{
			if((playerShips[i].transform.position - this.transform.position).magnitude <= PlayerNoticeRadius) return true;
		}
		return false;
	}
	
	public bool ShouldAttackPlayerShips()
	{
		List<Agent> playerShips = Manager.Friends;
		for(int i = 0; i < playerShips.Count; i++)
		{
			if((playerShips[i].transform.position - this.transform.position).magnitude <= this.GetComponent<Ship>().AttackRadius) return true;
		}
		return false;
	}
}

/*if(Vector3.Distance(transform.position, origin_position) > max_displacement_radius) 
		{
			Vector3 direction = (origin_position - transform.position).normalized;
			transform.Translate(direction * Time.deltaTime * speedTimeConstant);
		}

		for(int i = 0; i < players.Count; i++) 
		{
			Vector3 playerPos = players[i].transform.position;

			// notice player
			if(Vector3.Distance(transform.position, playerPos) <= player_notice_radius) 
			{
				Vector3 direction = (playerPos - transform.position).normalized;
				transform.Translate(direction * Time.deltaTime * speedTimeConstant);
			}

			if(Vector3.Distance(transform.position, playerPos) <= shoot_radius) 
			{
				// shoot

				// decrease health
				players[i].GetComponents<Ship>()[0].TakeDamage(Time.deltaTime * hurtHealthTimeConstant);
			}
		}*/
