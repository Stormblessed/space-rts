﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour 
{
	public Vector3 MaxExtent;
	public Vector3 MinExtent;
	
	public List<Obstacle> Obstacles = new List<Obstacle>();
	public List<Agent> Friends = new List<Agent>();
	public List<Agent> Foes = new List<Agent>();
	public List<Goal> Goals = new List<Goal>();
	public Station MainStation;

	void Awake() 
	{
		Obstacles.AddRange(GameObject.FindObjectsOfType<Obstacle>());
		Friends.AddRange(FindAllAgentsOfType(ObjectType.Friend));
		Foes.AddRange(FindAllAgentsOfType(ObjectType.Foe));
		Goals.AddRange(GameObject.FindObjectsOfType<Goal>());
		MainStation = GameObject.FindObjectOfType<Station>();
		
		ScaleObtstacleCollidersByLargestAgent();
	}
	
	void Start()
	{
	}
	
	void Update() 
	{
	}
	
	public void RerouteAgents()
	{
		for(int i = 0; i < Friends.Count; i++)
		{
			Friends[i].DeterminePath();
		}
	}
	
	public void ScaleObtstacleCollidersByLargestAgent()
	{
		float greatestAgentRadius = GetGreatestAgentRadius();
		for(int i = 0; i < Obstacles.Count; i++)
		{
			Obstacle obstacle = Obstacles[i];
			float worldSpaceRadius = obstacle.GetComponent<SphereCollider>().bounds.extents.magnitude;
			float radiusRatio = greatestAgentRadius / worldSpaceRadius;
			obstacle.GetComponent<SphereCollider>().radius += radiusRatio;
		}
	}
	
	public float GetGreatestAgentRadius()
	{
		float greatestRadius = 0;
		for(int i = 0; i < Friends.Count; i++)
		{
			SphereCollider agentCollider = Friends[i].GetComponent<SphereCollider>();
			if(agentCollider.bounds.extents.magnitude > greatestRadius) greatestRadius = agentCollider.bounds.extents.magnitude;
		}
		return greatestRadius;
	}
	
	public Vector3 GetFlockCenter()
	{
		Vector3 avgCenter = new Vector3();
		
		for(int i = 0; i < Friends.Count; i++)
		{
			if(i == 0) avgCenter = Friends[i].transform.position;
			else avgCenter += Friends[i].transform.position;
		}
		
		if(Friends.Count != 0) avgCenter /= Friends.Count;
		
		return avgCenter;
	}
	
	public bool IsCollision(Vector3 point)
	{
		foreach(Obstacle obstacle in Obstacles)
		{
			if(obstacle.Contains(point)) return true;
		}
		return false;
	}
	
	public bool Intersects(Vector3 posA, Vector3 posB)
	{
		RaycastHit hit;
		return Physics.Raycast(posA, Vector3.Normalize(posB - posA), out hit);
	}
	
	public List<Agent> FindAllAgentsOfType(ObjectType type)
	{
		List<Agent> allAgents = new List<Agent>();
		allAgents.AddRange(GameObject.FindObjectsOfType<Agent>());
		List<Agent> agentsOfType = new List<Agent>();
		for(int i = 0; i < allAgents.Count; i++)
		{
			if(allAgents[i].Type == type) agentsOfType.Add(allAgents[i]);
		}
		return agentsOfType;
	}
}
