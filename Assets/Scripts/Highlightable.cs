﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Highlightable : MonoBehaviour 
{
	public ObjectType Type;
	private Color FriendColor = Color.green;
	private Color FoeColor = Color.red;
	private Color NeutralColor = Color.yellow;
	
	private List<Material> DefaultMaterials = new List<Material>();
	private bool Highlighted = false;
	
	void Start() 
	{
		List<Renderer> renderers = new List<Renderer>();
		renderers.AddRange(GetComponentsInChildren<Renderer>());
		
		for(int i = 0; i < renderers.Count; i++)
		{
			DefaultMaterials.Add(renderers[i].material);
        }
	}
	
	void Update() 
	{
	}
	
	void OnMouseOver()
	{
		if(!Highlighted) Highlight();
	}
	
	private void Highlight()
	{	
		List<Renderer> renderers = new List<Renderer>();
		renderers.AddRange(GetComponentsInChildren<Renderer>());
		
		for(int i = 0; i < renderers.Count; i++)
		{
			Material material = new Material(Shader.Find("BumpedOutline"));
			material.SetColor("_OutlineColor", GetObjectTypeColor());
			if(i < DefaultMaterials.Count && DefaultMaterials[i].shader.name != "Particles/Additive" && DefaultMaterials[i].shader.name != "Particles/Alpha Blended Premultiply")
			{
				material.SetColor("_Color", DefaultMaterials[i].color);
				renderers[i].material = material;
			}	
		}
		Highlighted = true;
	}
	
	void OnMouseExit()
	{
		if(Highlighted) Unhighlight();
	}
	
	private void Unhighlight()
	{	
		List<Renderer> renderers = new List<Renderer>();
		renderers.AddRange(GetComponentsInChildren<Renderer>());
		
		for(int i = 0; i < renderers.Count; i++)
		{
			if(i < DefaultMaterials.Count && DefaultMaterials[i].shader.name != "Particles/Additive" && DefaultMaterials[i].shader.name != "Particles/Alpha Blended Premultiply") 
			{
				renderers[i].material = DefaultMaterials[i];
				renderers[i].material.SetColor("_Color", DefaultMaterials[i].color);
			}
		}
		Highlighted = false;
	}
	
	private Color GetObjectTypeColor()
	{
		switch(Type)
		{
			case ObjectType.Friend:
				return FriendColor;
			case ObjectType.Foe:
				return FoeColor;
			case ObjectType.Neutral:
				return NeutralColor;
			default:
				return NeutralColor;
		}
	}
}
