﻿using UnityEngine;
using System.Collections;

public class LineCircle : MonoBehaviour 
{
	public float Radius = 1.0f;
	public int Vertices = 128;
	public float LineWidth = 0.05f;
	public Color Color = new Color(0.5f, 0.5f, 0.5f, 1.0f);
	
	private LineRenderer Circle;
	
	void Start() 
	{
		Circle = gameObject.GetComponent<LineRenderer>();
		
		Circle.material = new Material(Shader.Find("Particles/Additive"));
		Circle.SetColors(Color, Color);
		Circle.SetWidth(LineWidth, LineWidth);
		Circle.SetVertexCount(Vertices + 1);
		
		float deltaTheta = (float)(2.0 *  Mathf.PI) / Vertices;
		float theta = 0;
		
		for(int i = 0; i < Vertices + 1; i++)
		{
			float x = Radius * Mathf.Cos(theta);
			float z = Radius * Mathf.Sin(theta);
			Vector3 position = new Vector3(x + transform.position.x, 0.0f, z + transform.position.z);
			
			Circle.SetPosition(i, position);
			
			theta += deltaTheta;
		}
	}
	
	void Update() 
	{
	}
	
	public void MoveTo(Vector3 position)
	{
		float deltaTheta = (float)(2.0 *  Mathf.PI) / Vertices;
		float theta = 0;
		
		for(int i = 0; i < Vertices + 1; i++)
		{
			float x = Radius * Mathf.Cos(theta);
			float z = Radius * Mathf.Sin(theta);
			Vector3 pointPos = new Vector3(x + position.x, position.y, z + position.z);
			
			Circle.SetPosition(i, pointPos);
			
			theta += deltaTheta;
		}
	}
}
