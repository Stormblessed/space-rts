﻿using UnityEngine;

public enum ObjectType
{
	Friend,
	Foe,
	Neutral
}