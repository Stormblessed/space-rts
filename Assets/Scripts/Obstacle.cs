﻿using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour 
{
	private SphereCollider Collider;

	void Start() 
	{
		Collider = gameObject.GetComponent<SphereCollider>();
	}
	
	void Update() 
	{
	}
	
	public bool Contains(Vector3 point)
	{
		return Collider.bounds.Contains(point);
	}
}
