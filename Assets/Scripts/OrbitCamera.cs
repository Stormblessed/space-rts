﻿/*
 * Thanks Unity Community Wiki!
 */

using UnityEngine;
using System.Collections;

public class OrbitCamera : MonoBehaviour
{
	public Transform Target;
	public Vector3 TargetOffset;
	public float Distance = 5.0f;
	public float MaxDistance = 20;
	public float MinDistance = .6f;
	public float XSpeed = 200.0f;
	public float YSpeed = 200.0f;
	public int YMinLimit = -80;
	public int YMaxLimit = 80;
	public int ZoomRate = 40;
	public float PanSpeed = 0.3f;
	public float ZoomDampening = 5.0f;
	
	private float XDeg = 0.0f;
	private float YDeg = 0.0f;
	private float CurrentDistance;
	private float DesiredDistance;
	private Quaternion CurrentRotation;
	private Quaternion DesiredRotation;
	private Quaternion Rotation;
	private Vector3 Position;
	
	private GameManager Manager;
	private CommandManager Commander;
	
	private Vector3 RightClick = -Vector2.one;
	
	void Start() 
	{ 
		Init(); 
		Manager = GameObject.FindObjectOfType<GameManager>();
		Commander = GameObject.FindObjectOfType<CommandManager>();
	}
	
	void OnEnable() 
	{ 
		Init(); 
	}
	
	public void Init()
	{
		if(!Target)
		{
			GameObject go = new GameObject("Camera Target");
			go.transform.position = transform.position + (transform.forward * Distance);
			Target = go.transform;
		}
		
		Distance = Vector3.Distance(transform.position, Target.position);
		CurrentDistance = Distance;
		DesiredDistance = Distance;
		
		Position = transform.position;
		Rotation = transform.rotation;
		CurrentRotation = transform.rotation;
		DesiredRotation = transform.rotation;
		
		XDeg = Vector3.Angle(Vector3.right, transform.right );
		YDeg = Vector3.Angle(Vector3.up, transform.up );
	}
	
	void Update()
	{
		if(Commander.IsStationSelected()) Target.position = Manager.MainStation.transform.position;
		else if (Commander.GetNumberOfSelectedAgents() <= 0) Target.position = Manager.GetFlockCenter();
		else Target.position = Commander.GetSelectedAgentsCenter();
	}
	
	void LateUpdate()
	{
		if(Input.GetMouseButtonDown(1)) RightClick = Input.mousePosition;
		if(Input.GetMouseButtonUp(1)) RightClick = -Vector2.one;
		
		if(Input.GetMouseButton(2) && Input.GetKey(KeyCode.LeftAlt) && Input.GetKey(KeyCode.LeftControl)) //Zoom
		{
			DesiredDistance -= Input.GetAxis("Mouse Y") * Time.deltaTime * ZoomRate * 0.125f * Mathf.Abs(DesiredDistance);
		}
		else if(Input.GetMouseButton(1)) //Orbit
		{
			if(RightClick != -Vector3.one && (RightClick - Input.mousePosition).magnitude > 5.0f)
			{
				XDeg += Input.GetAxis("Mouse X") * XSpeed * 0.02f;
				YDeg -= Input.GetAxis("Mouse Y") * YSpeed * 0.02f;
	
				YDeg = ClampAngle(YDeg, YMinLimit, YMaxLimit);
	
				DesiredRotation = Quaternion.Euler(YDeg, XDeg, 0);
				CurrentRotation = transform.rotation;
				
				Rotation = Quaternion.Lerp(CurrentRotation, DesiredRotation, Time.deltaTime * ZoomDampening);
				transform.rotation = Rotation;
			}
		}
		else if(Input.GetMouseButton(2)) //Pan
		{
			Target.rotation = transform.rotation;
			Target.Translate(Vector3.right * -Input.GetAxis("Mouse X") * PanSpeed);
			Target.Translate(transform.up * -Input.GetAxis("Mouse Y") * PanSpeed, Space.World);
		}
		
		DesiredDistance -= Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime * ZoomRate * Mathf.Abs(DesiredDistance);
		DesiredDistance = Mathf.Clamp(DesiredDistance, MinDistance, MaxDistance);
		CurrentDistance = Mathf.Lerp(CurrentDistance, DesiredDistance, Time.deltaTime * ZoomDampening);

		Position = Target.position - (Rotation * Vector3.forward * CurrentDistance + TargetOffset);
		transform.position = Position;
	}
		
	private static float ClampAngle(float angle, float min, float max)
	{
		if(angle < -360)
			angle += 360;
		if(angle > 360)
			angle -= 360;
		return Mathf.Clamp(angle, min, max);
	}
}



