﻿using UnityEngine;
using System.Collections;

public class OreAsteroid : MonoBehaviour 
{
	public float MaxEther = 5000.0f;
	private float CurrentEther = 5000.0f;
	
	private CommandManager Commander;
	
	private bool MouseHover = false;
	
	void Start()
	{
		Commander = GameObject.FindObjectOfType<CommandManager>();
	}
	
	void Update() 
	{
	}
	
	void OnGUI()
	{
		if(MouseHover)
		{
			Vector2 targetPosition = Camera.main.WorldToScreenPoint(transform.position);						
			GUI.color = Color.yellow;
			GUI.Box(new Rect(targetPosition.x - 40, Screen.height - targetPosition.y - 45, 80, 30), CurrentEther.ToString());
			GUI.color = Color.white;
			GUI.Box(new Rect(targetPosition.x - 40, Screen.height - targetPosition.y - 60, 80, 30), "Ether");
			GUI.color = Color.white;
		}
	}
	
	void OnMouseOver()
	{
		MouseHover = true;
		if(Input.GetMouseButtonUp(1))
		{
			Commander.IssueHarvestCommand(this);
		}
	}
	
	void OnMouseExit()
	{
		MouseHover = false;
	}
	
	public float AttemptHarvest(float amount)
	{
		float amountHarvested = 0.0f;
		if(CurrentEther > 0.0f)
		{
			float diff = CurrentEther - amount;
			if(diff >= 0)
			{
				amountHarvested = amount;
				CurrentEther = diff;
			}
			else
			{
				amountHarvested = amount + diff;
				CurrentEther = 0;
			}
		}
		return amountHarvested;
	}
}
