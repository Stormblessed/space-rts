﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathManager : MonoBehaviour 
{
	public int NumberOfNodes = 500;
	
	public List<PathNode> PathNodes = new List<PathNode>();
	
	private GameManager Manager;

	void Awake() 
	{
	}
	
	void Start()
	{
		Manager = GameObject.FindObjectOfType<GameManager>();
		GeneratePathMap(NumberOfNodes);
	}
	
	void Update() 
	{
	}
	
	public void GeneratePathMap(int numNodes)
	{
		GeneratePathNodes(numNodes);
		GenerateNodePaths();
	}
	
	public void GeneratePathNodes(int numNodes)
	{
		PathNodes.Clear();
		
		int count = 0;
		while(count < numNodes)
		{
			float xRand = Random.Range(Manager.MinExtent.x, Manager.MaxExtent.x);
			float yRand = Random.Range(Manager.MinExtent.y, Manager.MaxExtent.y);
			float zRand = Random.Range(Manager.MinExtent.z, Manager.MaxExtent.z);
			
			if(Manager.IsCollision(new Vector3(xRand, yRand, zRand))) continue;
			
			PathNodes.Add(new PathNode(new Vector3(xRand, yRand, zRand)));
			count++;
		}
		
		//PathNodes.Add(new PathNode(Manager.Goals[0].transform.position));
	}
	
	public void GenerateNodePaths()
	{
		for(int i = 0; i < PathNodes.Count; i++)
		{
			for(int j = 0; j < PathNodes.Count; j++)
			{
				if(j == i) continue;
				if(!Manager.Intersects(PathNodes[i].Position, PathNodes[j].Position)) 
					PathNodes[i].Neighbors.Add(PathNodes[j]);
			}
		}
	}
	
	public PathNode GetTopNode(List<PathNode> nodes)
	{
		float currentSmallestTotalValue = float.MaxValue;
		PathNode currentTopNode = nodes[0]; //default;
		for(int i = 0; i < nodes.Count; i++)
		{
			float totalValue = nodes[i].TotalCost;
			if(totalValue < currentSmallestTotalValue)
			{
				currentSmallestTotalValue = totalValue;
				currentTopNode = nodes[i];
			}
		}
		//Write a heuristic function, walk through each node and grab the one with the greatest heuristic val + PathLength
		//Write a PathLength function for PathNode that walks through the Parent chain and adds up the distances between them.
		return currentTopNode;
    }
    
	public PathNode GetClosestPathNode(Vector3 point)
	{
		float ShortestDistance = float.MaxValue;
		PathNode currentClosestNode = PathNodes[0]; //Default value
		for(int i = 0; i < PathNodes.Count; i++)
		{
			float distance = Vector3.Magnitude(point - PathNodes[i].Position);
			if(distance <= ShortestDistance)
			{
				ShortestDistance = distance;
				currentClosestNode = PathNodes[i];
            }
        }
        return currentClosestNode;
    }
    
    public void Reset()
    {
    	for(int i = 0; i < PathNodes.Count; i++)
    	{
    		PathNodes[i].Reset();
    	}
    }
    
    void OnDrawGizmos()
    {
		for(int i = 0; i < PathNodes.Count; i++)
		{
			PathNode node = PathNodes[i];
			
			Gizmos.color = Color.blue;
			for(int j = 0; j < node.Neighbors.Count; j++)
			{
				//Gizmos.DrawLine(node.Position, node.Neighbors[j].Position);
			}
			Gizmos.color = Color.yellow;
			Gizmos.DrawSphere(PathNodes[i].Position, 0.5f);
		}
	}
}
