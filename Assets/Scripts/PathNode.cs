﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathNode
{
	public Vector3 Position;
	public List<PathNode> Neighbors = new List<PathNode>();
	public PathNode Parent;
	
	public float PathCost = 0;
	public float TotalCost = 0;
	
	public PathNode() {}
	public PathNode(float x, float y, float z) {Position = new Vector3(x, y, z);}
	public PathNode(Vector3 position) {Position = position;}
	
	public float DistanceToPoint(Vector3 point)
	{
		return Vector3.Magnitude(Position - point);
	}
	
	public void Reset()
	{
		PathCost = 0;
		TotalCost = 0;
		Parent = null;
	}
}
