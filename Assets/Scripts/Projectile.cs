﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour 
{
	public Agent Target;
	
	public float DefaultMoveSpeed = 20.0f;
	public float Damage = 20.0f;
	
	public float Lifetime = 10.0f;
	private float Age = 0.0f;
	
	private Vector3 lastDirection;
	
	void Start() 
	{
	}
	
	void Update() 
	{		
		Age += Time.deltaTime;
		if(Age >= Lifetime) Die();
		else if(HitTarget()) Detonate();
		else PursueTarget();
	}
	
	private bool HitTarget()
	{
		if(Target == null) return false;
		else return (transform.position - Target.transform.position).magnitude <= 1.0f;
		return false;
	}
	
	private void Detonate()
	{
		if(Target == null) Die();
		else
		{
			Target.GetComponent<Ship>().TakeDamage(Damage);
			Die();
		}
	}
	
	private void PursueTarget()
	{
		if(Target != null)
		{
			Vector3 heading = (Target.transform.position - transform.position);
			Vector3 direction = heading / heading.magnitude;
			
			Vector3 overallForce = direction * DefaultMoveSpeed * Time.deltaTime;
			transform.Translate(overallForce);
			lastDirection = direction;
		}
		else
			transform.Translate(lastDirection * DefaultMoveSpeed * Time.deltaTime);
	}
	
	private void Die()
	{
		Destroy(this.gameObject);
	}
}
