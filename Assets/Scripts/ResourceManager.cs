﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ResourceManager : MonoBehaviour 
{
	public float TotalEther = 0.0f;
	
	public Text EtherAmountText;
	
	void Start()
	{
	}
	
	void Update() 
	{
		if(Input.GetKeyDown(KeyCode.P)) TotalEther = 10000;
	}
	
	void OnGUI()
	{
		EtherAmountText.text = TotalEther.ToString("00.00");
	}
}
