﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Ship : MonoBehaviour 
{
	public ObjectType Type = ObjectType.Friend;
	public string Class;
	
	public GameObject Harvester;
	
	private GameManager Manager;
	private ResourceManager ResourceManager;
	private CommandManager Commander;
	
	public float MaxHealth = 100.0f;
	private float CurrentHealth;
	
	public float MaxShields = 50.0f;
	private float CurrentShields;
	public float ShieldRechargeRate = 5.0f;
	public float ShieldRechargeDelay = 5.0f;
	private float ShieldRechargeTimer = 0.0f;
	
	public float HarvestRate = 10.0f;
	
	private bool MouseHover = false;
	
	public List<Agent> AttackTargets = new List<Agent>();
	public float AttackRadius = 50.0f;
	
	public float DistanceMarkerDeterminant = 150.0f;
	
	public Projectile LaserPrefab;
	public float WeaponRange = 35.0f;
	public float WeaponFireInterval = 3.0f;
	private float WeaponFireCooldown = 0.0f;
	
	void Start() 
	{
		Manager = GameObject.FindObjectOfType<GameManager>();
		ResourceManager = GameObject.FindObjectOfType<ResourceManager>();
		Commander = GameObject.FindObjectOfType<CommandManager>();
		if(Harvester != null) Harvester.SetActive(false);
		
		CurrentHealth = MaxHealth;
		CurrentShields = MaxShields;
	}
	
	void Update() 
	{
		if(WeaponFireCooldown > 0.0f) WeaponFireCooldown -= Time.deltaTime;
		else if(ShouldFireWeapon()) FireWeapon();
	}
	
	private bool ShouldFireWeapon()
	{	
		Agent shipAgent = this.GetComponent<Agent>();
		return shipAgent.Status == AgentStatus.Attacking && WeaponFireCooldown <= 0.0f && TargetInRange(WeaponRange);
	}
	
	private void FireWeapon()
	{
		Agent shipAgent = this.GetComponent<Agent>();
		Projectile projectile = Instantiate(LaserPrefab, transform.position, Quaternion.identity) as Projectile;
		if(AttackTargets.Count > 0) projectile.Target = shipAgent.GetClosestAgent(AttackTargets);
		else projectile.GetComponent<Projectile>().Target = shipAgent.GetClosestAgent(Manager.FindAllAgentsOfType(shipAgent.GetOppositeType()));
		
		WeaponFireCooldown += WeaponFireInterval;
	}
	
	private bool TargetInRange(float range)
	{
		for(int i = 0; i < AttackTargets.Count; i++)
		{
			if(AttackTargets[i] == null) continue;
			else if((AttackTargets[i].transform.position - transform.position).magnitude <= range) return true;
		}
		return false;
	}
	
	void OnGUI()
	{
		if(MouseHover)
		{
			Vector2 targetPosition = Camera.main.WorldToScreenPoint(transform.position);						
			GUI.color = Color.white;
			GUI.Box(new Rect(targetPosition.x - 40, Screen.height - targetPosition.y - 45, 80, 30), this.Class);
		}
		if(ShouldDrawDistanceMarkers()) DrawDistanceMarkers();
		if(ShouldShowHealthBars()) DrawHealthBars();
	}
	
	private bool ShouldDrawDistanceMarkers()
	{
		return (Camera.main.transform.position - transform.position).magnitude >= DistanceMarkerDeterminant;
	}
	
	private void DrawDistanceMarkers()
	{
		Vector2 targetPosition = Camera.main.WorldToScreenPoint(transform.position);
		
		Texture2D markerTexture;
		if(Type == ObjectType.Friend) markerTexture = Resources.Load("transparent") as Texture2D;
		else markerTexture = Resources.Load("enemy_x") as Texture2D;
		
		if(Type == ObjectType.Friend) GUI.color = Color.green;
		else GUI.color = Color.red;
		
		GUI.DrawTexture(new Rect(targetPosition.x - 4, Screen.height - targetPosition.y - 4, 8, 8), markerTexture);
		
		GUI.color = Color.white;
	}
	
	private bool ShouldShowHealthBars()
	{
		return MouseHover || Commander.Contains(this.GetComponent<Agent>());
	}
	
	private void DrawHealthBars()
	{
		Vector2 targetPosition = Camera.main.WorldToScreenPoint(transform.position);
		
		Texture2D healthBarTexture = Resources.Load("healthbar_texture") as Texture2D;
		
		GUI.color = Color.red;
		GUI.DrawTexture(new Rect(targetPosition.x - 15, Screen.height - targetPosition.y - 20, 30, 3), healthBarTexture);
		GUI.color = Color.green;
		GUI.DrawTexture(new Rect(targetPosition.x - 15, Screen.height - targetPosition.y - 20, GetHealthBarPercent() * 30, 3), healthBarTexture);
		GUI.color = Color.yellow;
		GUI.DrawTexture(new Rect(targetPosition.x - 15, Screen.height - targetPosition.y - 15, (CurrentShields / MaxShields) * 30, 3), healthBarTexture);
		GUI.color = Color.white;
	}
	
	private float GetHealthBarPercent()
	{
		if(CurrentHealth >= 0) return CurrentHealth / MaxHealth;
		else return 0;
	}

	public void TakeDamage(float amount) 
	{
		CurrentHealth -= amount;
		if(CurrentHealth <= 0) Die();
	}
	
	private void Die()
	{
		if(Commander.Contains(this.GetComponent<Agent>())) Commander.Remove(this.GetComponent<Agent>());
		if(Type == ObjectType.Friend) Manager.Friends.Remove(this.GetComponent<Agent>());
		else Manager.Foes.Remove(this.GetComponent<Agent>());
		
		for(int i = 0; i < Manager.Friends.Count; i++)
		{
			if(Manager.Friends[i].GetComponent<Ship>().AttackTargets.Contains(this.GetComponent<Agent>())) 
				Manager.Friends[i].GetComponent<Ship>().AttackTargets.Remove(this.GetComponent<Agent>());
		}
		
		for(int i = 0; i < Manager.Foes.Count; i++)
		{
			if(Manager.Foes[i].GetComponent<Ship>().AttackTargets.Contains(this.GetComponent<Agent>())) 
				Manager.Foes[i].GetComponent<Ship>().AttackTargets.Remove(this.GetComponent<Agent>());
		}
		
		Destroy(this.gameObject);
	}
	
	void OnMouseDown()
	{
		Commander.HandleShipClicked(this);
	}
	
	void OnMouseOver()
	{
		MouseHover = true;
	}
	
	void OnMouseExit()
	{
		MouseHover = false;
	}
	
	public void Harvest(OreAsteroid asteroid)
	{
		if(Harvester != null)
		{
			if(Harvester.activeSelf == false) Harvester.SetActive(true);
			float amountHarvested = asteroid.AttemptHarvest(HarvestRate * Time.deltaTime);
			if(amountHarvested > 0) ResourceManager.TotalEther += amountHarvested;
			else 
			{
				this.GetComponent<Agent>().Status = AgentStatus.NoGoal;
				StopHarvesting();
			}
		}
	}
	
	public void StopHarvesting()
	{
		if(Harvester != null) Harvester.SetActive(false);
	}
}
