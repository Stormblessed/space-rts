﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SpawnManager : MonoBehaviour 
{
	public StationSpawner EyeSpawner;
	public StationSpawner SwordSpawner;
	
	public Button EyeshipButton;
	public Button SwordshipButton;
	
	private ResourceManager ResourceManager;
	
	void Start() 
	{
		EyeshipButton.onClick.AddListener(() => {SpawnEyeship();});
		SwordshipButton.onClick.AddListener(() => {SpawnSwordship();});
		
		ResourceManager = GameObject.FindObjectOfType<ResourceManager>();
	}
	
	void Update() 
	{
		if(Input.GetKeyDown(KeyCode.E)) SpawnEyeship();
	}
	
	void OnGUI()
	{
		if(EyeSpawner.CheckPurchase(ResourceManager.TotalEther) < 0) EyeshipButton.interactable = false;
		else EyeshipButton.interactable = true;
		if(SwordSpawner.CheckPurchase(ResourceManager.TotalEther) < 0) SwordshipButton.interactable = false;
		else SwordshipButton.interactable = true;
	}
	
	public void SpawnEyeship()
	{
		float etherAfterPurchaseAttempt = EyeSpawner.CheckPurchase(ResourceManager.TotalEther);
		if(etherAfterPurchaseAttempt >= 0) 
		{
			EyeSpawner.AddSpawnRequest("Eye");
			ResourceManager.TotalEther = etherAfterPurchaseAttempt;
		}
	}
	
	public void SpawnSwordship()
	{
		float etherAfterPurchaseAttempt = SwordSpawner.CheckPurchase(ResourceManager.TotalEther);
		if(etherAfterPurchaseAttempt >= 0) 
		{
			SwordSpawner.AddSpawnRequest("Sword");
			ResourceManager.TotalEther = etherAfterPurchaseAttempt;
        }
	}
}
