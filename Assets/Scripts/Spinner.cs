﻿using UnityEngine;
using System.Collections;

public class Spinner : MonoBehaviour 
{
	public Vector3 SpinRate = new Vector3(0, 0, 50.0f);
	void Start() 
	{
	}
	
	void Update() 
	{
		this.transform.Rotate(SpinRate * Time.deltaTime);
	}
}
