﻿using UnityEngine;
using System.Collections;

public class Station : MonoBehaviour 
{
	private CommandManager Commander;
	
	public float MaxHealth = 100.0f;
	private float CurrentHealth = 100.0f;
	
	public float MaxShields = 50.0f;
	private float CurrentShields = 50.0f;
	public float ShieldRechargeRate = 5.0f;
	public float ShieldRechargeDelay = 5.0f;
	private float ShieldRechargeTimer = 0.0f;
	
	private bool MouseHover = false;
	
	void Start() 
	{
		Commander = GameObject.FindObjectOfType<CommandManager>();
	}
	
	void Update() 
	{
	}
	
	void OnGUI()
	{
		if(ShouldShowHealthBars()) DrawHealthBars();
	}
	
	private bool ShouldShowHealthBars()
	{
		return MouseHover || Commander.IsStationSelected(this);
	}
	
	private void DrawHealthBars()
	{
		Vector2 targetPosition = Camera.main.WorldToScreenPoint(transform.position);
		
		Texture2D healthBarTexture = Resources.Load("healthbar_texture") as Texture2D;
		
		GUI.color = Color.red;
		GUI.DrawTexture(new Rect(targetPosition.x - 15, Screen.height - targetPosition.y - 20, 30, 3), healthBarTexture);
		GUI.color = Color.green;
		GUI.DrawTexture(new Rect(targetPosition.x - 15, Screen.height - targetPosition.y - 20, (CurrentHealth / MaxHealth) * 30, 3), healthBarTexture);
		GUI.color = Color.yellow;
		GUI.DrawTexture(new Rect(targetPosition.x - 15, Screen.height - targetPosition.y - 15, (CurrentShields / MaxShields) * 30, 3), healthBarTexture);
		GUI.color = Color.white;
	}
	
	void OnMouseDown()
	{
		Commander.SelectStation(this);
	}
	
	void OnMouseOver()
	{
		MouseHover = true;
	}
	
	void OnMouseExit()
	{
		MouseHover = false;
	}
}
