﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StationSpawner : MonoBehaviour 
{
	public GameObject ShipPrefab;
	public float ShipCost = 0.0f;
	
	private List<string> SpawnRequests = new List<string>();
	
	public float SpawnDelay = 2.5f;
	private float SpawnCooldown = 0.0f;
	
	private GameManager Manager;
	
	void Start() 
	{
		Manager = GameObject.FindObjectOfType<GameManager>();
	}
	
	void Update() 
	{
		if(SpawnCooldown <= 0.0f) TrySpawn();
		HandleSpawnCooldown();
	}
	
	private void HandleSpawnCooldown()
	{
		if(SpawnCooldown >= 0)
			SpawnCooldown -= Time.deltaTime;
		else SpawnCooldown = 0.0f;
	}
	
	private void TrySpawn()
	{
		if(SpawnRequests.Count > 0) HandleSpawn();
	}
	
	private void HandleSpawn()
	{
		string request = SpawnRequests[0];
		SpawnRequests.RemoveAt(0);
		
		GameObject ShipObject = Instantiate(ShipPrefab, this.transform.position, Quaternion.identity) as GameObject;
		ShipObject.GetComponent<Agent>().Status = AgentStatus.Spawning;
		ShipObject.GetComponent<Agent>().SpawnMovementDirection = (((transform.position + transform.forward * 100.0f) - transform.position).normalized);
		if(ShipObject.GetComponent<Agent>().Type == ObjectType.Friend) Manager.Friends.Add(ShipObject.GetComponent<Agent>());
		else if(ShipObject.GetComponent<Agent>().Type == ObjectType.Foe) Manager.Foes.Add(ShipObject.GetComponent<Agent>());
		
		SpawnCooldown += SpawnDelay;
	}
	
	public void AddSpawnRequest(string shipClass)
	{
		SpawnRequests.Add(shipClass);
	}
	
	public float CheckPurchase(float totalResouces)
	{
		return totalResouces - ShipCost;
	}
}
