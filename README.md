# README #

Space Station Defense!
A Homeworld-like 3-dimensional Space RTS.
Enemy Eggships approach! Collect resources from Ether asteroids and build ships to defend the Space Station! 
Uses a boids-based forces system and a PRM + Weighted A* for ship behavior and pathing. Features shader-swapping for friend/foe/neutral highlighting and both click and click-drag(box) unit selection.